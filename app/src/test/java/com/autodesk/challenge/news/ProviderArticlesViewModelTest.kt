package com.autodesk.challenge.news

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.autodesk.challenge.news.model.Article
import com.autodesk.challenge.news.model.repository.ArticlesRepository
import com.autodesk.challenge.news.viewmodel.ProviderArticlesViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProviderArticlesViewModelTest {

    private val providerArticlesRepository: ArticlesRepository = mock()
    private val observer: Observer<List<Article>> = mock()
    private lateinit var providerArticlesViewModel: ProviderArticlesViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        providerArticlesViewModel = ProviderArticlesViewModel(providerArticlesRepository)
    }

    @Test
    fun articlesNotNull() {
        whenever(providerArticlesRepository.getArticles()).thenReturn(MutableLiveData())
        Assert.assertNotNull(providerArticlesViewModel.getProviderArticles())
    }

    @Test
    fun articlesNotEmpty() {
        val liveData = MutableLiveData<List<Article>>()
        liveData.value = listOf(
            Article(Any(), "author", "title1", "desc", "url", "imageUrl", "pub", "cont"),
            Article(Any(), "author", "title2", "desc", "url", "imageUrl", "pub", "cont")
        )

        whenever(providerArticlesRepository.getArticles()).thenReturn(liveData)
        Assert.assertTrue(providerArticlesViewModel.getProviderArticles().value!!.isNotEmpty())
    }

    @Test
    fun articlesDataChanged() {
        val liveData = MutableLiveData<List<Article>>()
        val articles = listOf(
            Article(Any(), "author", "title1", "desc", "url", "imageUrl", "pub", "cont"),
            Article(Any(), "author", "title2", "desc", "url", "imageUrl", "pub", "cont")
        )
        liveData.value = articles

        whenever(providerArticlesRepository.getArticles()).thenReturn(liveData)
        providerArticlesViewModel.getProviderArticles().observeForever(observer)
        verify(observer).onChanged(articles)
    }

}