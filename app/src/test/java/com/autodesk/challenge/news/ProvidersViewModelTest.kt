package com.autodesk.challenge.news

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.autodesk.challenge.news.model.Provider
import com.autodesk.challenge.news.model.repository.ProvidersRepository
import com.autodesk.challenge.news.viewmodel.ProvidersViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProvidersViewModelTest {

    private val providersRepository : ProvidersRepository = mock()
    private val observer: Observer<List<Provider>> = mock()
    private lateinit var providersViewModel : ProvidersViewModel

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        providersViewModel = ProvidersViewModel(providersRepository)
    }

    @Test
    fun providersNotNull() {
        whenever(providersRepository.getProviders()).thenReturn(MutableLiveData())
        Assert.assertNotNull(providersViewModel.getProviders())
    }

    @Test
    fun providersNotEmpty() {
        val liveData = MutableLiveData<List<Provider>>()
        liveData.value = listOf(
            Provider("id", "name1", "desc", "url", "cat", "lang", "country"),
            Provider("id", "name2", "desc", "url", "cat", "lang", "country")
        )

        whenever(providersRepository.getProviders()).thenReturn(liveData)
        Assert.assertTrue(providersViewModel.getProviders().value!!.isNotEmpty())
    }

    @Test
    fun providersDataChanged() {
        val liveData = MutableLiveData<List<Provider>>()
        val providers = listOf(
            Provider("id", "name1", "desc", "url", "cat", "lang", "country"),
            Provider("id", "name2", "desc", "url", "cat", "lang", "country")
        )
        liveData.value = providers

        whenever(providersRepository.getProviders()).thenReturn(liveData)
        providersViewModel.getProviders().observeForever(observer)
        verify(observer).onChanged(providers)
    }

}