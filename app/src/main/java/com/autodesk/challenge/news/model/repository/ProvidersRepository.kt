package com.autodesk.challenge.news.model.repository

import androidx.lifecycle.MutableLiveData
import com.autodesk.challenge.news.model.Provider

interface ProvidersRepository {
    fun getProviders(): MutableLiveData<List<Provider>>
}