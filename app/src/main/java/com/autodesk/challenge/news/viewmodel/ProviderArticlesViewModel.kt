package com.autodesk.challenge.news.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.autodesk.challenge.news.model.Article
import com.autodesk.challenge.news.model.repository.ArticlesRepository

/**
 * Manages the provider's articles list data
 */
class ProviderArticlesViewModel(private val providerArticlesRepository: ArticlesRepository) : ViewModel() {

    private val providerArticles: MutableLiveData<List<Article>> by lazy {
        providerArticlesRepository.getArticles()
    }

    fun getProviderArticles(): LiveData<List<Article>> {
        return providerArticles
    }

    fun refreshData() {
        providerArticlesRepository.getArticles()
    }

    class ProviderArticlesViewModelFactory(private val providerArticlesRepository: ArticlesRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(ArticlesRepository::class.java)
                .newInstance(providerArticlesRepository)
        }
    }
}