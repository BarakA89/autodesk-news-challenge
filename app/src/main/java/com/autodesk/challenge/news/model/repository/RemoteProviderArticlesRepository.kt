package com.autodesk.challenge.news.model.repository

import androidx.lifecycle.MutableLiveData
import com.autodesk.challenge.news.model.Article
import com.autodesk.challenge.news.model.Provider
import com.autodesk.challenge.news.model.network.ArticlesResponse
import com.autodesk.challenge.news.model.network.NewsApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteProviderArticlesRepository(private val provider: Provider) : ArticlesRepository {

    private val newsApiService = NewsApiService.create()
    private val articles = MutableLiveData<List<Article>>()

    override fun getArticles() : MutableLiveData<List<Article>> {
        newsApiService.getArticles(provider.id).enqueue(object : Callback<ArticlesResponse?> {
            override fun onFailure(call: Call<ArticlesResponse?>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ArticlesResponse?>, response: Response<ArticlesResponse?>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        articles.value = it.articles
                    }
                }
            }
        })
        return articles
    }

}