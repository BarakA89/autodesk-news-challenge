package com.autodesk.challenge.news.view


import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.autodesk.challenge.news.R
import com.autodesk.challenge.news.model.Article
import kotlinx.android.synthetic.main.fragment_article.view.*

/**
 * Displays an article
 */
class ArticleFragment : Fragment() {

    private lateinit var article: Article

    companion object {
        private const val ARTICLE_ARG = "article_arg"

        fun newInstance(article: Article) = ArticleFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARTICLE_ARG, article)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        article = arguments?.getParcelable(ARTICLE_ARG)!!
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_article, container, false)

        val articleWebView: WebView = view.articleWebView
        val articleProgressBar: ProgressBar? = view.articleProgressBar
        articleWebView.settings.javaScriptEnabled = true
        articleWebView.webViewClient = object: WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                articleProgressBar?.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                articleProgressBar?.visibility = View.GONE
            }
        }
        articleWebView.webChromeClient = object: WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                if (articleProgressBar?.visibility == View.VISIBLE) {
                    articleProgressBar.progress = newProgress
                }
            }
        }
        articleWebView.loadUrl(article.url)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity as AppCompatActivity?
        activity?.let {
            val toolbar: Toolbar = activity.findViewById(R.id.toolbar)
            activity.setSupportActionBar(toolbar)

            val actionBar: ActionBar? = activity.supportActionBar
            actionBar?.let {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.setDisplayShowTitleEnabled(true)
                actionBar.title = article.title
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            activity?.onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }


}
