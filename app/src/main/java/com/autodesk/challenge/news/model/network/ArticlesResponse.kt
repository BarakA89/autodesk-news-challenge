package com.autodesk.challenge.news.model.network

import com.autodesk.challenge.news.model.Article

data class ArticlesResponse(
    val status: String,
    val totalResults: Int,
    val articles: List<Article>
)