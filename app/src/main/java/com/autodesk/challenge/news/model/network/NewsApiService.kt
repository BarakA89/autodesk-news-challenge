package com.autodesk.challenge.news.model.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiService {

    @GET("v2/sources")
    fun getProviders() : Call<ProvidersResponse>

    @GET("v2/everything")
    fun getArticles(@Query("sources") sources: String) : Call<ArticlesResponse>

    companion object {

        private const val apiKey = "c203c0a9f7cb4068a48f4d7895ba4a73"

        fun create(): NewsApiService {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor { chain: Interceptor.Chain ->
                    chain.run {
                        proceed(
                            request().newBuilder()
                                .addHeader("X-Api-Key", apiKey)
                                .build()
                        )
                    }
                }.build()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://newsapi.org/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(NewsApiService::class.java)
        }

    }
}