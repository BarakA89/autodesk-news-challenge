package com.autodesk.challenge.news.model.repository

import androidx.lifecycle.MutableLiveData
import com.autodesk.challenge.news.model.Provider
import com.autodesk.challenge.news.model.network.NewsApiService
import com.autodesk.challenge.news.model.network.ProvidersResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteProvidersRepository : ProvidersRepository {

    private val newsApiService = NewsApiService.create()
    private val providers = MutableLiveData<List<Provider>>()

    override fun getProviders(): MutableLiveData<List<Provider>> {
        newsApiService.getProviders().enqueue(object : Callback<ProvidersResponse?> {
            override fun onFailure(call: Call<ProvidersResponse?>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ProvidersResponse?>, response: Response<ProvidersResponse?>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        providers.value = it.sources
                    }
                }
            }
        })
        return providers
    }
}