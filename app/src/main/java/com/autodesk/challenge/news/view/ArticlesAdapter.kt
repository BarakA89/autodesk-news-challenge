package com.autodesk.challenge.news.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.autodesk.challenge.news.R
import com.autodesk.challenge.news.model.Article
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.article_list_item.view.*

/**
 * Displays the articles list
 */
class ArticlesAdapter : RecyclerView.Adapter<ArticlesAdapter.NewsViewHolder>() {

    private var articles: List<Article>? = null
    private var onArticleClickedListener: OnArticleClickedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.article_list_item, parent, false)

        return NewsViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val article = articles?.get(position)

        article?.let {
            Glide.with(holder.itemView.context).load(it.urlToImage).into(holder.articleImageView)
            holder.articlePublishDateTextView.text = it.publishedAt // TODO: Format date
            holder.articleTitleTextView.text = it.title
            holder.itemView.setOnClickListener {
                onArticleClickedListener?.onArticleClickedListener(article)
            }
        }
    }

    override fun getItemCount(): Int = articles?.size ?: 0

    fun setArticles(articles: List<Article>) {
        this.articles = articles
        notifyDataSetChanged()
    }

    fun setOnArticleClickedListener(listener: OnArticleClickedListener) {
        this.onArticleClickedListener = listener
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val articleImageView: ImageView = itemView.articleImageView
        val articlePublishDateTextView: TextView = itemView.articlePublishDateTextView
        val articleTitleTextView: TextView = itemView.articleTitleTextView
    }

    /**
     * Callback for when an article is clicked.
     */
    interface OnArticleClickedListener {

        /**
         * Called when an article is clicked.
         *
         * @param article The article that was clicked.
         */
        fun onArticleClickedListener(article: Article)
    }
}
