package com.autodesk.challenge.news.model.network

import com.autodesk.challenge.news.model.Provider

data class ProvidersResponse(
    val status: String,
    val sources: List<Provider>
)
