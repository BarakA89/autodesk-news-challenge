package com.autodesk.challenge.news.model.repository

import androidx.lifecycle.MutableLiveData
import com.autodesk.challenge.news.model.Article

interface ArticlesRepository {
    fun getArticles() : MutableLiveData<List<Article>>
}