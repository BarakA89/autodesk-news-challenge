package com.autodesk.challenge.news.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.autodesk.challenge.news.R
import com.autodesk.challenge.news.model.Provider
import kotlinx.android.synthetic.main.provider_list_item.view.*

/**
 * Displays the news providers list
 */
class ProvidersAdapter : RecyclerView.Adapter<ProvidersAdapter.ProviderViewHolder>() {

    private var providers: List<Provider>? = null
    private var onProviderClickedListener: OnProviderClickedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.provider_list_item, parent, false)

        return ProviderViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProviderViewHolder, position: Int) {
        val provider = providers?.get(position)

        provider?.let {
            holder.providerNameTextView.text = it.name
            holder.itemView.setOnClickListener {
                onProviderClickedListener?.onProviderClickedListener(provider)
            }
        }
    }

    override fun getItemCount(): Int = providers?.size ?: 0

    fun setProviders(providers: List<Provider>?) {
        this.providers = providers
        notifyDataSetChanged()
    }

    fun setOnProviderClickedListener(listener: OnProviderClickedListener) {
        this.onProviderClickedListener = listener
    }

    class ProviderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val providerNameTextView: TextView = itemView.providerNameTextView
    }

    /**
     * Callback for when a provider is clicked.
     */
    interface OnProviderClickedListener {

        /**
         * Called when a provider is clicked.
         *
         * @param provider The provider that was clicked.
         */
        fun onProviderClickedListener(provider: Provider)
    }

}
