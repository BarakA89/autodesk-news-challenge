package com.autodesk.challenge.news.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.autodesk.challenge.news.R
import com.autodesk.challenge.news.model.Provider
import com.autodesk.challenge.news.model.repository.RemoteProvidersRepository
import com.autodesk.challenge.news.viewmodel.ProvidersViewModel
import kotlinx.android.synthetic.main.fragment_providers.view.*

/**
 * Displays a list of news providers.
 */
class ProvidersFragment : Fragment(), ProvidersAdapter.OnProviderClickedListener {

    private lateinit var viewModel: ProvidersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val providersRepository = RemoteProvidersRepository()
        val providersViewModelFactory = ProvidersViewModel.ProvidersViewModelFactory(providersRepository)
        viewModel = ViewModelProviders.of(this, providersViewModelFactory).get(ProvidersViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_providers, container, false)

        val providersProgressBar = view.providersProgressBar

        val providersAdapter = ProvidersAdapter()
        providersAdapter.setOnProviderClickedListener(this)

        val providersRecyclerView = view.providersRecyclerView
        val linearLayoutManager = LinearLayoutManager(activity)
        providersRecyclerView.layoutManager = linearLayoutManager
        providersRecyclerView.adapter = providersAdapter
        providersRecyclerView.addItemDecoration(DividerItemDecoration(activity, linearLayoutManager.orientation))

        viewModel.getProviders().observe(viewLifecycleOwner, Observer<List<Provider>?> {
            providersAdapter.setProviders(it)
            providersProgressBar.visibility = View.GONE
        })

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity as AppCompatActivity?
        activity?.let {
            val toolbar: Toolbar = activity.findViewById(R.id.toolbar)
            activity.setSupportActionBar(toolbar)

            val actionBar: ActionBar? = activity.supportActionBar
            actionBar?.let {
                actionBar.setDisplayHomeAsUpEnabled(false)
                actionBar.setDisplayShowTitleEnabled(true)
                actionBar.title = getString(R.string.providers_fragment_title)
            }
        }
    }

    override fun onProviderClickedListener(provider: Provider) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.fragment_container, ProviderArticlesFragment.newInstance(provider))
            ?.addToBackStack(null)
            ?.commit()
    }


}
