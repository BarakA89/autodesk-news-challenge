package com.autodesk.challenge.news.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.autodesk.challenge.news.model.Provider
import com.autodesk.challenge.news.model.repository.ProvidersRepository

/**
 * Manages the providers list data
 */
class ProvidersViewModel(repository: ProvidersRepository) : ViewModel() {

    private val providers: MutableLiveData<List<Provider>> by lazy {
        repository.getProviders()
    }

    fun getProviders(): LiveData<List<Provider>> {
        return providers
    }

    class ProvidersViewModelFactory(private val repository: ProvidersRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(ProvidersRepository::class.java).newInstance(repository)
        }
    }

}