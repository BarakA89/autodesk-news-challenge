package com.autodesk.challenge.news.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.autodesk.challenge.news.R
import com.autodesk.challenge.news.model.Article
import com.autodesk.challenge.news.model.Provider
import com.autodesk.challenge.news.model.repository.RemoteProviderArticlesRepository
import com.autodesk.challenge.news.viewmodel.ProviderArticlesViewModel
import kotlinx.android.synthetic.main.fragment_provider_articles.view.*


/**
 * Displays news articles from a specific provider.
 */
class ProviderArticlesFragment : Fragment(), ArticlesAdapter.OnArticleClickedListener {

    private lateinit var provider: Provider
    private lateinit var viewModel: ProviderArticlesViewModel

    companion object {
        private const val PROVIDER_ARG = "provider_arg"

        fun newInstance(provider: Provider) = ProviderArticlesFragment().apply {
            arguments = Bundle().apply {
                putParcelable(PROVIDER_ARG, provider)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        provider = arguments?.getParcelable(PROVIDER_ARG)!!
        val articlesRepository = RemoteProviderArticlesRepository(provider)
        val articlesViewModelFactory = ProviderArticlesViewModel.ProviderArticlesViewModelFactory(articlesRepository)
        viewModel = ViewModelProviders.of(this, articlesViewModelFactory).get(ProviderArticlesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_provider_articles, container, false)

        val providerArticlesProgressBar = view.providerArticlesProgressBar

        val articlesAdapter = ArticlesAdapter()
        articlesAdapter.setOnArticleClickedListener(this)

        val articlesRecyclerView = view.articlesRecyclerView
        articlesRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        articlesRecyclerView.adapter = articlesAdapter

        viewModel.getProviderArticles().observe(viewLifecycleOwner, Observer {
            articlesAdapter.setArticles(it)
            articlesRecyclerView.scrollToPosition(0)
            providerArticlesProgressBar.visibility = View.GONE
        })

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity as AppCompatActivity?
        activity?.let {
            val toolbar: Toolbar = activity.findViewById(R.id.toolbar)
            activity.setSupportActionBar(toolbar)

            val actionBar: ActionBar? = activity.supportActionBar
            actionBar?.let {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.setDisplayShowTitleEnabled(true)
                actionBar.title = provider.name
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.refreshData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            activity?.onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onArticleClickedListener(article: Article) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.fragment_container, ArticleFragment.newInstance(article))
            ?.addToBackStack(null)
            ?.commit()
    }

}
